package com.basytyan.placeautocomplete.views;


import com.basytyan.placeautocomplete.model.Place;

import java.util.ArrayList;

/**
 * Created by Basytyan K Pratama on 17/11/2016.
 */
public interface NearbyView {
    void showProgressDialogIndicator();
    void onFailed(String message);
    void onSuccess(ArrayList<Place> arrayList);
}
