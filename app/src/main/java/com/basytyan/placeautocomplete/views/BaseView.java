package com.basytyan.placeautocomplete.views;

import android.content.Context;

/**
 * Created by Basytyan K Pratama on 17/11/2016.
 */
public interface BaseView {
    Context getContext();
}
