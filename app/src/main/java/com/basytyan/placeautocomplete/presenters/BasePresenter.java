package com.basytyan.placeautocomplete.presenters;

/**
 * Created by Basytyan K Pratama on 1/18/2016.
 */
public interface BasePresenter<V> {

    void attachView(V view);

    void detachView();
}
