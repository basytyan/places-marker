package com.basytyan.placeautocomplete.api;

/**
 * Created by Basytyan K Pratama on 17/11/2016.
 */

public interface ApiConfig {

    String BASE_URL = "https://maps.googleapis.com";

    String URL_NEARBY = "/maps/api/place/nearbysearch/json?";
}
