package com.basytyan.placeautocomplete.api;

import com.basytyan.placeautocomplete.model.PlaceResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Basytyan K Pratama on 17/11/2016.
 */

public interface ApiService {

    @GET(ApiConfig.URL_NEARBY)
    Observable<PlaceResponse> getNearby(@Query("location") String location, @Query("radius") String radius,
                                        @Query("type") String type, @Query("key") String apikey);
}
