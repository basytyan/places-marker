package com.basytyan.placeautocomplete.dagger;


import com.basytyan.placeautocomplete.dagger.modules.AppModule;
import com.basytyan.placeautocomplete.dagger.modules.NetworkModule;
import com.basytyan.placeautocomplete.presenters.NearbyPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Basytyan K Pratama on 1/18/2016.
 */
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {
    void inject(App app);
    void inject(NearbyPresenter app);

}
