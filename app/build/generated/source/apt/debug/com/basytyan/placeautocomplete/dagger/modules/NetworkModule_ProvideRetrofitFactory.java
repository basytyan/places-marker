package com.basytyan.placeautocomplete.dagger.modules;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkModule_ProvideRetrofitFactory implements Factory<Retrofit> {
  private final NetworkModule module;
  private final Provider<HttpUrl> httpUrlProvider;
  private final Provider<OkHttpClient> okHttpClientProvider;

  public NetworkModule_ProvideRetrofitFactory(NetworkModule module, Provider<HttpUrl> httpUrlProvider, Provider<OkHttpClient> okHttpClientProvider) {  
    assert module != null;
    this.module = module;
    assert httpUrlProvider != null;
    this.httpUrlProvider = httpUrlProvider;
    assert okHttpClientProvider != null;
    this.okHttpClientProvider = okHttpClientProvider;
  }

  @Override
  public Retrofit get() {  
    Retrofit provided = module.provideRetrofit(httpUrlProvider.get(), okHttpClientProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<Retrofit> create(NetworkModule module, Provider<HttpUrl> httpUrlProvider, Provider<OkHttpClient> okHttpClientProvider) {  
    return new NetworkModule_ProvideRetrofitFactory(module, httpUrlProvider, okHttpClientProvider);
  }
}

