package com.basytyan.placeautocomplete.presenters;

import com.basytyan.placeautocomplete.api.ApiService;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NearbyPresenter_MembersInjector implements MembersInjector<NearbyPresenter> {
  private final Provider<ApiService> apiServiceProvider;

  public NearbyPresenter_MembersInjector(Provider<ApiService> apiServiceProvider) {  
    assert apiServiceProvider != null;
    this.apiServiceProvider = apiServiceProvider;
  }

  @Override
  public void injectMembers(NearbyPresenter instance) {  
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiService = apiServiceProvider.get();
  }

  public static MembersInjector<NearbyPresenter> create(Provider<ApiService> apiServiceProvider) {  
      return new NearbyPresenter_MembersInjector(apiServiceProvider);
  }
}

