package com.basytyan.placeautocomplete.dagger.modules;

import com.google.gson.Gson;
import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkModule_ProvideGsonFactory implements Factory<Gson> {
  private final NetworkModule module;

  public NetworkModule_ProvideGsonFactory(NetworkModule module) {  
    assert module != null;
    this.module = module;
  }

  @Override
  public Gson get() {  
    Gson provided = module.provideGson();
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<Gson> create(NetworkModule module) {  
    return new NetworkModule_ProvideGsonFactory(module);
  }
}

