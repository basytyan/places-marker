// Generated code from Butter Knife. Do not modify!
package com.basytyan.placeautocomplete.views.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.basytyan.placeautocomplete.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MapsActivity_ViewBinding<T extends MapsActivity> implements Unbinder {
  protected T target;

  private View view2131689660;

  private View view2131689659;

  private View view2131689657;

  private View view2131689658;

  @UiThread
  public MapsActivity_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.containerListPLace = Utils.findRequiredViewAsType(source, R.id.container_list_place, "field 'containerListPLace'", FrameLayout.class);
    view = Utils.findRequiredView(source, R.id.btn_atm, "method 'atmListener'");
    view2131689660 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.atmListener();
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_hospital, "method 'hospitalListener'");
    view2131689659 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.hospitalListener();
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_gas, "method 'gasListener'");
    view2131689657 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.gasListener();
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_restaurant, "method 'restaurantListener'");
    view2131689658 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.restaurantListener();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.containerListPLace = null;

    view2131689660.setOnClickListener(null);
    view2131689660 = null;
    view2131689659.setOnClickListener(null);
    view2131689659 = null;
    view2131689657.setOnClickListener(null);
    view2131689657 = null;
    view2131689658.setOnClickListener(null);
    view2131689658 = null;

    this.target = null;
  }
}
