package com.basytyan.placeautocomplete.dagger;

import com.basytyan.placeautocomplete.api.ApiService;
import com.basytyan.placeautocomplete.dagger.modules.AppModule;
import com.basytyan.placeautocomplete.dagger.modules.NetworkModule;
import com.basytyan.placeautocomplete.dagger.modules.NetworkModule_ProvideApiServiceFactory;
import com.basytyan.placeautocomplete.dagger.modules.NetworkModule_ProvideHttpUrlFactory;
import com.basytyan.placeautocomplete.dagger.modules.NetworkModule_ProvideOkHttpClientFactory;
import com.basytyan.placeautocomplete.dagger.modules.NetworkModule_ProvideRetrofitFactory;
import com.basytyan.placeautocomplete.presenters.NearbyPresenter;
import com.basytyan.placeautocomplete.presenters.NearbyPresenter_MembersInjector;
import dagger.MembersInjector;
import dagger.internal.MembersInjectors;
import dagger.internal.ScopedProvider;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class DaggerAppComponent implements AppComponent {
  private Provider<HttpUrl> provideHttpUrlProvider;
  private Provider<OkHttpClient> provideOkHttpClientProvider;
  private Provider<Retrofit> provideRetrofitProvider;
  private Provider<ApiService> provideApiServiceProvider;
  private MembersInjector<NearbyPresenter> nearbyPresenterMembersInjector;

  private DaggerAppComponent(Builder builder) {  
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {  
    return new Builder();
  }

  private void initialize(final Builder builder) {  
    this.provideHttpUrlProvider = ScopedProvider.create(NetworkModule_ProvideHttpUrlFactory.create(builder.networkModule));
    this.provideOkHttpClientProvider = ScopedProvider.create(NetworkModule_ProvideOkHttpClientFactory.create(builder.networkModule));
    this.provideRetrofitProvider = ScopedProvider.create(NetworkModule_ProvideRetrofitFactory.create(builder.networkModule, provideHttpUrlProvider, provideOkHttpClientProvider));
    this.provideApiServiceProvider = ScopedProvider.create(NetworkModule_ProvideApiServiceFactory.create(builder.networkModule, provideRetrofitProvider));
    this.nearbyPresenterMembersInjector = NearbyPresenter_MembersInjector.create(provideApiServiceProvider);
  }

  @Override
  public void inject(App app) {  
    MembersInjectors.noOp().injectMembers(app);
  }

  @Override
  public void inject(NearbyPresenter app) {  
    nearbyPresenterMembersInjector.injectMembers(app);
  }

  public static final class Builder {
    private AppModule appModule;
    private NetworkModule networkModule;
  
    private Builder() {  
    }
  
    public AppComponent build() {  
      if (appModule == null) {
        throw new IllegalStateException("appModule must be set");
      }
      if (networkModule == null) {
        this.networkModule = new NetworkModule();
      }
      return new DaggerAppComponent(this);
    }
  
    public Builder appModule(AppModule appModule) {  
      if (appModule == null) {
        throw new NullPointerException("appModule");
      }
      this.appModule = appModule;
      return this;
    }
  
    public Builder networkModule(NetworkModule networkModule) {  
      if (networkModule == null) {
        throw new NullPointerException("networkModule");
      }
      this.networkModule = networkModule;
      return this;
    }
  }
}

