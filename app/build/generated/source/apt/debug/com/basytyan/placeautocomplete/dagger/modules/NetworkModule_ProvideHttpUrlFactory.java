package com.basytyan.placeautocomplete.dagger.modules;

import dagger.internal.Factory;
import javax.annotation.Generated;
import okhttp3.HttpUrl;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkModule_ProvideHttpUrlFactory implements Factory<HttpUrl> {
  private final NetworkModule module;

  public NetworkModule_ProvideHttpUrlFactory(NetworkModule module) {  
    assert module != null;
    this.module = module;
  }

  @Override
  public HttpUrl get() {  
    HttpUrl provided = module.provideHttpUrl();
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<HttpUrl> create(NetworkModule module) {  
    return new NetworkModule_ProvideHttpUrlFactory(module);
  }
}

