// Generated code from Butter Knife. Do not modify!
package com.basytyan.placeautocomplete.views.adapters;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.basytyan.placeautocomplete.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PlaceListAdapter$PlaceListHolder_ViewBinding<T extends PlaceListAdapter.PlaceListHolder> implements Unbinder {
  protected T target;

  @UiThread
  public PlaceListAdapter$PlaceListHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.txtName = Utils.findRequiredViewAsType(source, R.id.txt_name, "field 'txtName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.txtName = null;

    this.target = null;
  }
}
