package com.basytyan.placeautocomplete.dagger.modules;

import dagger.internal.Factory;
import javax.annotation.Generated;
import okhttp3.OkHttpClient;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {
  private final NetworkModule module;

  public NetworkModule_ProvideOkHttpClientFactory(NetworkModule module) {  
    assert module != null;
    this.module = module;
  }

  @Override
  public OkHttpClient get() {  
    OkHttpClient provided = module.provideOkHttpClient();
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<OkHttpClient> create(NetworkModule module) {  
    return new NetworkModule_ProvideOkHttpClientFactory(module);
  }
}

